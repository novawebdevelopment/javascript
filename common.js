var v1 = document.getElementById("v1");
var v2 = document.getElementById("v2");
var v3 = document.getElementById("v3");
var v4 = document.getElementById("v4");

window.addEventListener("keydown", toggleVoices, false);

var canvas = document.getElementById("soundscape");
var ctx = canvas.getContext("2d");

// Now get the height and width of window so that
// it works on every resolution. Yes! on mobiles too.
W = window.innerWidth;   // 1855
H = window.innerHeight;  //  985

// Set the canvas to occupy FULL space.
// canvas.width = W;
// canvas.height = H;

W = canvas.width;
H = canvas.height;

canvas.addEventListener("mousedown",  mouseDown, false);
canvas.addEventListener("mousemove",  mouseXY,   false);
canvas.addEventListener("touchstart", touchDown, false);
canvas.addEventListener("touchmove",  touchXY,   true);
canvas.addEventListener("touchend",   touchUp,   false);

var radius = canvas.height / 2;
// 0 is at 3:00, and angles go counter-intuitive (counter-clockwise)
var FrontLeft  = {x:Math.cos(1.25*Math.PI)*radius,
                  y:Math.sin(1.25*Math.PI)*radius,
                  color:"#afffff"};
var FrontRight = {x:Math.cos(1.75*Math.PI)*radius,
                  y:Math.sin(1.75*Math.PI)*radius,
                  color:"#afffff"};
var RearRight  = {x:Math.cos(0.25*Math.PI)*radius,
                  y:Math.sin(0.25*Math.PI)*radius,
                  color:"#afffff"};
var RearLeft   = {x:Math.cos(0.75*Math.PI)*radius,
                  y:Math.sin(0.75*Math.PI)*radius,
                  color:"#afffff"};

ctx.translate(radius, radius);    // Center the origin

// Most of what follws (and some of the above) from:
// http://cssdeck.com/labs/canvas-mouse-cursor-leaving-colorful-circle-trails
// See also:
// https://stackoverflow.com/questions/23373975/how-to-change-the-origin-of-the-canvas-to-the-center

// RequestAnimFrame: a browser API for getting smooth animations
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          window.oRequestAnimationFrame      ||
          window.msRequestAnimationFrame     ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();

// Some variables for later use
var circles      = [],
    circlesCount = 20,
    mouse        = {},
    mouseIsDown  = 0;

// function which will paint the canvas black.
//
// Default fillStyle is also black but specifying it
// won't hurt anyone and we can change it back later.
// If you want more controle over colors, then declare
// them in a variable.

function paintCanvas() {
  ctx.globalCompositeOperation = "source-over"; // Over-paint
  ctx.fillStyle = "#cfffff";
  ctx.fillRect(-radius, -radius, W, H);
  ctx.strokeStyle = "#007f00";
  ctx.fillStyle   = "#afffff";
  ctx.beginPath();
  ctx.arc(0, 0, radius, 0, 2*Math.PI); // Center (x, y), radius, start, end
  ctx.stroke();
  ctx.fill();
  ctx.strokeStyle = "#7f0000";
  ctx.beginPath();
  ctx.fillStyle = FrontLeft.color;
  ctx.arc(FrontLeft.x,  FrontLeft.y,  20, 0, 2*Math.PI);
  ctx.stroke();
  ctx.fill();
  ctx.beginPath();
  ctx.fillStyle = FrontRight.color;
  ctx.arc(FrontRight.x, FrontRight.y, 20, 0, 2*Math.PI);
  ctx.stroke();
  ctx.fill();
  ctx.beginPath();
  ctx.fillStyle = RearRight.color;
  ctx.arc(RearRight.x,  RearRight.y,  20, 0, 2*Math.PI);
  ctx.stroke();
  ctx.fill();
  ctx.beginPath();
  ctx.fillStyle = RearLeft.color;
  ctx.arc(RearLeft.x,  RearLeft.y,   20, 0, 2*Math.PI);
  ctx.stroke();
  ctx.fill();
}

// This will act as a class which we will use to create
// circle objects. Also, remember that class names are
// generally started with a CAPITAL letter and are
// singular
function Circle() {
  this.x = Math.random() * W;
  this.y = Math.random() * H;
  this.r = 2;

  // Generate a random color
  this.red   = Math.floor(Math.random() * 255);
  this.green = Math.floor(Math.random() * 255);
  this.blue  = Math.floor(Math.random() * 255);
  this.color = "rgb("+ this.red +", "+ this.green +", "+ this.blue +")";

  this.draw = function() {
    ctx.globalCompositeOperation = "lighter";
    ctx.globalCompositeOperation = "source-over";
    ctx.beginPath();
    ctx.fillStyle = this.color;
    ctx.arc(this.x, this.y, this.r, 0, Math.PI*2, false);
    ctx.fill();
    ctx.closePath();
  }
}

// Insert a random circle to the circles array.
for(var i = 0; i < circlesCount; i++) {
  circles.push(new Circle());
}

// A function that will be called in the loop, so
// consider it as the `main` function
function draw() {
paintCanvas();

  for(i = 0; i < circles.length; i++) {
    var c1 = circles[i],
    c2 = circles[i-1];

    circles[circles.length - 1].draw();

    if(mouse.x && mouse.y) {
      circles[circles.length - 1].x = mouse.x;
      circles[circles.length - 1].y = mouse.y;
      c1.draw();
    }

    if(i > 0) {
      c2.x += (c1.x - c2.x) * 0.6;
      c2.y += (c1.y - c2.y) * 0.6;
    }
  }
}

// The handlers

function toggleVoices(e) {
  e.preventDefault();
  if (e.keyCode == 32) {
    if (v1.paused) {
      v1.play();
      v2.play();
      v3.play();
      v4.play();
    }
    else {
      v1.pause();
      v2.pause();
      v3.pause();
      v4.pause();
    }
  }
}

function distance(x, y) {
  return Math.min((Math.sqrt(Math.pow(x,2) + Math.pow(y,2)) / 400), 1);
}

function mouseXY(e) {
  e.preventDefault();
  var rect = canvas.getBoundingClientRect();
  mouse.x = e.clientX - rect.left - radius;
  mouse.y = e.clientY - rect.top  - radius;
  this.red   =  175 + Math.floor(distance(mouse.x, mouse.y) *  80);
  this.green =  255 - Math.ceil(distance(mouse.x, mouse.y)  * 255);
  this.blue  =  255 - Math.ceil(distance(mouse.x, mouse.y)  * 255);
  if (((mouse.x < 0) && (mouse.x > -400)) &&
      ((mouse.y < 0) && (mouse.y > -400))) {
    v1.volume = 0.5 + (distance(mouse.x, mouse.y) / 2.0);
    v2.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v3.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v4.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    FrontLeft.color  = "rgb("+ this.red +", "+ this.green +", "+ this.blue +")";
    FrontRight.color = "#afffff";
    RearRight.color  = "#afffff";
    RearLeft.color   = "#afffff";
  }
  else if (((mouse.x > 0) && (mouse.x <  400)) &&
           ((mouse.y < 0) && (mouse.y > -400))) {
    v1.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v2.volume = 0.5 + (distance(mouse.x, mouse.y) / 2.0);
    v3.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v4.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    FrontLeft.color  = "#afffff";
    FrontRight.color = "rgb("+ this.red +", "+ this.green +", "+ this.blue +")";
    RearRight.color  = "#afffff";
    RearLeft.color   = "#afffff";
  }
  else if (((mouse.x > 0) && (mouse.x < 400)) &&
           ((mouse.y > 0) && (mouse.y < 400))) {
    v1.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v2.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v3.volume = 0.5 + (distance(mouse.x, mouse.y) / 2.0);
    v4.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    FrontLeft.color  = "#afffff";
    FrontRight.color = "#afffff";
    RearRight.color  = "rgb("+ this.red +", "+ this.green +", "+ this.blue +")";
    RearLeft.color   = "#afffff";
  }
  else if (((mouse.x < 0) && (mouse.x > -400)) &&
           ((mouse.y > 0) && (mouse.y <  400))) {
    v1.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v2.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v3.volume = 0.5 - (distance(mouse.x, mouse.y) / 2.0);
    v4.volume = 0.5 + (distance(mouse.x, mouse.y) / 2.0);
    FrontLeft.color  = "#afffff";
    FrontRight.color = "#afffff";
    RearRight.color  = "#afffff";
    RearLeft.color   = "rgb("+ this.red +", "+ this.green +", "+ this.blue +")";
  }
//      console.log("Mouse = (" + mouse.x + ", " + mouse.y + ")"); // DEBUG
}

function mouseUp() {
  mouseIsDown = 0;
  mouseXY();
}

function mouseDown() {
  mouseIsDown = 1;
  mouseXY();
}

function touchXY(e) {
  e.preventDefault();
  mouse.x = e.targetTouches[0].pageX - canvas.offsetLeft;
  mouse.y = e.targetTouches[0].pageY - canvas.offsetTop;
//      console.log("Touch = (" + mouse.x + ", " + mouse.y + ")"); // DEBUG
}

function touchUp() {
  mouseIsDown = 0;
}

function touchDown() {
  mouseIsDown = 1;
  touchXY();
}

// The loop
function animloop() {
  draw();
  requestAnimFrame(animloop);
}

animloop();
