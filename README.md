# README #

Welcome to **Sound Advice**.

### Background ###

People who have experienced a hearing loss as adults often have
difficulty learning to use assistive listening devices such as hearing
aids and cochlear implants. Users of the equipment often have
unrealistic expectations regarding the quality of the audio signal
provided, and become frustrated with the rehabilitation exercises.

**Sound Advice** is an attempt to _gamify_ the experience, in the
hopes that it will allow users to stick with the exercises long enough
to derive some benefit from the devices, rather than abandoning them.
Technologies involved:

* Symbolic Sound's **Paca** Digital Signal Processor (DSP) together
  with t.c. electronics konnekt-24D digital to analog converter (DAC)
  and Kyma 7 control software
* HTML5 and Javascript for the web-based version

### How do I get set up? ###

Not much to do at this point, really.

* Clone the repository.
* If you have a web server (Apache, Nginx, or other) running, move the
  repository to a web-accessible directory, and point your web browser
  at it with a URL like http://localhost/... Otherwise, simply point
  your web browser to a URL like file:///... (Your web browser should
  have a "File -> Open File..." or similar in a pull-down menu, that
  you can use to navigate to the repository directory.)
* Dependencies: A browser that understands HTML5, Javascript, and Ogg
  Vorbis audio files. Most modern web browsers qualify.

### Who do I talk to? ###

* Kevin Cole

### Miscellaneous ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
